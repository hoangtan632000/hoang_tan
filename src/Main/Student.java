/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Student {
    private int id,age;
    private String name,address,sex;

    public Student() {
        super();
    }

    public Student(int id, int age, String name, String address, String sex) {
        super();
        this.id = id;
        this.age = age;
        this.name = name;
        this.address = address;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getSex() {
        return sex;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    
    public void inputInfo(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập mã sinh viên");
        id = Integer.parseInt(scanner.nextLine());
        System.out.println("Nhập tên");
        name = scanner.nextLine();
        System.out.println("Nhập quê quán");
        address = scanner.nextLine();
        System.out.println("Nhập giới tính");
        sex = scanner.nextLine();
        System.out.println("Nhập tuổi");
        age = Integer.parseInt(scanner.nextLine());
    }
    
    public void showInfo(){
        System.out.println("Mã sinh viên:"+id);
        System.out.println("Tên sinh viên:"+name);
        System.out.println("Quê quán:"+address);
        System.out.println("Giới tính:"+sex);
        System.out.println("Tuổi:"+age);
    }

    boolean equalsIgnoreCase(String thái_Nguyên) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
