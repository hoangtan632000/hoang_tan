/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;


import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Admin
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<ListSV> arrList = new ArrayList<>();
        int n;
        ListSV listSV;
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        
        for(int i=0;i<n;i++){
            System.out.println("Nhập thông tin học sinh thứ " + (i + 1) + ": ");
            listSV = new ListSV();
            listSV.inputListSV();
            arrList.add(listSV);
        }
        
        for (int i = 0; i < arrList.size(); i++) {
            System.out.println("\nThông tin học sinh thứ " + (i+1) + ": ");
            arrList.get(i).showListSV();
        }
        
        System.out.println("\nNhững Học Sinh nữ là: ");
        for (int i = 0; i < arrList.size(); i++) {
            if (arrList.get(i).getStudent().getSex().equalsIgnoreCase("nữ")) {
                arrList.get(i).showListSV();
            }
        }
        
        System.out.println("\nNhững Học Sinh tên là Tuấn là: ");
        for (int i = 0; i < arrList.size(); i++) {
            if (arrList.get(i).getStudent().getName().equalsIgnoreCase("Tuấn")) {
                arrList.get(i).showListSV();
            }
        }
    }
    
}
